﻿var loadFile = (event, imageId) => {
    $(`#${imageId}`).attr('src', URL.createObjectURL(event.target.files[0]));
    $(`#${imageId}`).onload = function () {
        URL.revokeObjectURL($(`#${imageId}`).src);
    };
};

async function onFilePicked(event, id) {
    const imageName = $(`#${id}-image-name`).val();
    const file = $(`#${id}-image-upload`)[0].files[0];
    if (file != null) {
        await uploadBannerImageFileAsync(id, imageName, file);
    }
};

async function uploadBannerImageFileAsync(id, imageName, file) {
    const fileSize = file.size;
    const chunkSize = 2 * 1024;
    const chunksMaxCount = Math.floor((fileSize + chunkSize - 1) / chunkSize);

    const fileChunks = [];
    let offset = 0;
    for (let n = 0; n < chunksMaxCount; n++) {
        const fileChunk = file.slice(offset, offset + chunkSize);
        fileChunks.push(fileChunk);
        offset += fileChunk.size;
    }

    let chunk = undefined;
    offset = 0;
    while ((chunk = fileChunks.shift())) {
        const isEnd = offset + chunkSize > fileSize;
        await uploadFileChunkAsync(id, imageName, offset, chunk, isEnd);
        offset += chunkSize;
    }
}

async function uploadFileChunkAsync(id, imageName, offset, chunk, isEnd) {
    const base64Data = await blobToBase64Async(chunk);
    $.ajax({
        type: "POST",
        url: "/Commodity/UploadImage",
        data: {
            'id': id,
            'imageName': imageName,
            'offset': offset,
            'imageData': base64Data,
            'isEnd': isEnd
        },
        success: async () => {
            await waitForMe(100);
        }
    });
}

function waitForMe(milisec) {
    return new Promise(resolve => {
        setTimeout(() => { resolve(''); }, milisec);
    });
}

function blobToBase64Async(blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onloadend = function() {
            const tagBase64Str = reader.result;
            const mark = "base64,";
            const idx = tagBase64Str.indexOf(mark);
            const base64Str = tagBase64Str.substr(idx + mark.length);
            resolve(base64Str);
        };
        reader.readAsDataURL(blob);
    });
}