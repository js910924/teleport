using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Teleport.Models;

namespace Teleport.Proxy
{
    public class StockProxy : IStockProxy
    {
        private readonly ILogger<StockProxy> _logger;
        private const string YahooFinancePricePattern = @"<span class=""Trsdu\(0\.3s\) Fw\(b\) Fz\(36px\) Mb\(-4px\) D\(ib\)"" data-reactid=""32"">([0-9]*,?[0-9]*.[0-9]*)<\/span>";
        private const string YahooFinanceDailyChangePattern = @"<span class=""Trsdu\(0\.3s\) Fw\(500\) Pstart\(10px\) Fz\(24px\).*"" data-reactid=""33"">([\+*\-*][0-9]*\.[0-9]*) \(([\+*\-*][0-9]*\.[0-9]*)%\)<\/span>";
        private const int MaxRetryTimes = 3;

        private readonly HttpClient _httpClient;

        public StockProxy(IHttpClientFactory httpClientFactory, ILogger<StockProxy> logger)
        {
            _logger = logger;
            _httpClient = httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri("https://finance.yahoo.com/quote/");
        }

        public async Task<StockInfo> GetStockInfo(string stockSymbol)
        {
            // TODO: try Polly
            var retryCount = 0;
            while (retryCount++ < MaxRetryTimes)
            {
                var response = await _httpClient.GetAsync(stockSymbol);
                var html = await response.Content.ReadAsStringAsync();

                var priceRegex = new Regex(YahooFinancePricePattern);
                var dailyChangeRegex = new Regex(YahooFinanceDailyChangePattern);
                var priceMatch = priceRegex.Match(html);
                var dailyChangeMatch = dailyChangeRegex.Match(html);

                _logger.LogInformation($"GetStockInfo | priceMatch = {priceMatch}, changeMatch = {dailyChangeMatch}");
                if (priceMatch.Success && dailyChangeMatch.Success)
                {
                    _logger.LogInformation($"GetStockInfo | match success");
                    var price = Convert.ToDecimal(priceMatch.Groups[1].Value);
                    var change = Convert.ToDecimal(dailyChangeMatch.Groups[1].Value);
                    var percentageOfChange = Convert.ToDecimal(dailyChangeMatch.Groups[2].Value);

                    return new StockInfo {Symbol = stockSymbol, Price = price, PercentageOfChange = percentageOfChange / 100m, Change = change};
                }
            }

            _logger.LogInformation($"GetStockInfo | match failed");
            return new StockInfo() { Symbol = stockSymbol };
        }
    }
}