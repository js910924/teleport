﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Teleport.Models;
using Teleport.Services;

namespace Teleport.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StockTransactionController : BaseAuthorizeController
    {
        private readonly IStockTransactionService _stockTransactionService;

        public StockTransactionController(IStockTransactionService stockTransactionService)
        {
            _stockTransactionService = stockTransactionService;
        }

        [HttpPost]
        public async Task<OkResult> Add(StockTransactionDto stockTransactionDto)
        {
            var stockTransaction = stockTransactionDto.ToStockTransaction();
            stockTransaction.CustomerId = CustomerId;

            await _stockTransactionService.UpsertStockTransaction(stockTransaction);

            return Ok();
        }

        [HttpGet]
        public async Task<IEnumerable<StockTransactionDto>> GetAll()
        {
            var stockTransactions = await _stockTransactionService.GetStockTransactionsBy(CustomerId);

            return stockTransactions.Select(transaction => transaction.ToStockTransactionDto());
        }

        [HttpGet]
        public async Task<OkResult> Delete(int transactionId)
        {
            await _stockTransactionService.DeleteTransaction(transactionId, CustomerId);

            return Ok();
        }

        [HttpGet]
        public IEnumerable<string> GetAllStockTransactionActions()
        {
            return Enum.GetValues(typeof(StockAction))
                .Cast<StockAction>()
                .Select(action => action.ToString());
        }
    }
}