﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Teleport.Models;
using Teleport.Repository;
using Teleport.Services;

namespace Teleport.Controllers
{
    public class CommodityController : Controller
    {
        private readonly ICommodityService _commodityService;
        private readonly IImageRepository _imageRepository;

        public CommodityController(ICommodityService commodityService, IImageRepository imageRepository)
        {
            _commodityService = commodityService;
            _imageRepository = imageRepository;
        }

        public IActionResult Index()
        {
            var commodities = _commodityService.GetAllCommodities();

            var viewModel = new CommodityViewModel
            {
                Rows = commodities.Select(commodity => new CommodityRow
                {
                    Id = commodity.Id,
                    Title = commodity.Title,
                    Price = commodity.Price,
                    ImageName = commodity.ImageName
                }).OrderBy(commodity => commodity.Id).ToList()
            };

            return View("Index", viewModel);
        }

        [HttpPost]
        public IActionResult Upsert(CommodityOperationRequest request)
        {
            var commodity = new Commodity
            {
                Id = request.Id,
                Title = request.Title,
                Price = request.Price
            };
            _commodityService.UpsertCommodity(commodity);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Remove(int commodityId)
        {
            var commodity = new Commodity
            {
                Id = commodityId
            };

            _commodityService.RemoveCommodity(commodity);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult UploadImage(UploadImageDataRequest request)
        {
            _imageRepository.Upload(request);

            if (request.IsEnd)
            {
                var commodity =
                    _commodityService.GetAllCommodities()
                        .First(x => x.Id == request.Id);

                commodity.ImageName = request.ImageName;
                _commodityService.UpsertCommodity(commodity);
            }

            return RedirectToAction("Index");
        }
    }
}