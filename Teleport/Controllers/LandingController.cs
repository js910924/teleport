﻿using Microsoft.AspNetCore.Mvc;

namespace Teleport.Controllers
{
    public class LandingController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}