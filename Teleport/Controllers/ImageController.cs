﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Teleport.Models;
using Teleport.Repository;

namespace Teleport.Controllers
{
    public class ImageController : Controller
    {
        private readonly IImageRepository _imageRepository;

        public ImageController(IImageRepository imageRepository)
        {
            _imageRepository = imageRepository;
        }

        [HttpPost]
        public Task Upload(UploadImageDataRequest uploadImageDataRequest)
        {
            _imageRepository.Upload(uploadImageDataRequest);

            return Task.CompletedTask;
        }
    }
}