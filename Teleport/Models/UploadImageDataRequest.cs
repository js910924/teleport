﻿namespace Teleport.Models
{
    public class UploadImageDataRequest
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public int Offset { get; set; }
        public string ImageData { get; set; }
        public bool IsEnd { get; set; }
    }
}