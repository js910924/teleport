﻿using Teleport.Models;

namespace Teleport.Repository
{
    public interface IImageRepository
    {
        void Upload(UploadImageDataRequest uploadImageDataRequest);
    }
}