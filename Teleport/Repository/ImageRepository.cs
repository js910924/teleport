﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Teleport.Models;

namespace Teleport.Repository
{
    public class ImageRepository : IImageRepository
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ImageRepository(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public void Upload(UploadImageDataRequest uploadImageDataRequest)
        {
            EnsureDirectoryExist();

            var fileMode = FileMode.OpenOrCreate;
            if (uploadImageDataRequest.Offset == 0)
            {
                fileMode = FileMode.Create;
            }

            var dirPath = $"{_webHostEnvironment.ContentRootPath}/wwwroot/image/";
            using var file = new FileStream($"{dirPath}{uploadImageDataRequest.ImageName}.jpg", fileMode);
            file.Seek(uploadImageDataRequest.Offset, SeekOrigin.Begin);
            file.Write(Convert.FromBase64String(uploadImageDataRequest.ImageData));
            file.Flush();
            file.Close();
        }

        private void EnsureDirectoryExist()
        {
            if (!Directory.Exists($"{_webHostEnvironment.ContentRootPath}/wwwroot/image/"))
            {
                Directory.CreateDirectory($"{_webHostEnvironment.ContentRootPath}/wwwroot/image/");
            }
        }
    }
}